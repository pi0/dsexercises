module Stack where

data Stack t = Stack [t]
    deriving (Show)

--Top
top :: Stack t -> t
top (Stack (h:_) ) = h

--Pop
pop :: Stack t -> Stack t
pop (Stack (_:t)) = Stack t

--Push
push :: t -> Stack t -> Stack t
push v (Stack s) = Stack (v:s)

--Empty
empty :: Stack t -> Bool
empty (Stack []) = True
empty (Stack _) = False

--Convert List to Stack--
listToStack :: [t] -> Stack t
listToStack l = add l $ Stack [] where
    add [] s = s
    add (a:b) s = add b (push a s)