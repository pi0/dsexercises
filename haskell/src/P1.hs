module Main where


--Linked list
data DLinkedList a = DLinkedList { _curr :: Int , _first :: Int , _last :: Int , _dat :: [DLinkedListNode a] }

--Linked list node
data DLinkedListNode a= DLinkedListNode {_next :: Int , _prv:: Int , _val::a }

--Count
count :: DLinkedList a -> Int
count dl = length . filter ( _dat dl )

--Get  node[i]
get_node :: DLinkedList a -> Int -> DLinkedListNode a
get_node l i = (_dat l) !! i

--Get linked list's current node
curr_node :: DLinkedList a -> DLinkedListNode a
curr_node l = get_node l $ _curr l

--Get linked list's last node
last_node :: DLinkedList a -> DLinkedListNode a
last_node l = get_node l $ _last l

--Get linked list's current value
curr_val :: DLinkedList a -> a
curr_val l = _val.curr_node l

--nxt and prv
nxt :: DLinkedList a -> Int
nxt l = _next.curr_node l

prv :: DLinkedList a -> Int
prv l = _prv.curr_node l

--goNext and goPrv
goNext :: DLinkedList a -> DLinkedList a
goNext l = DLinkedList {_curr=nxt l,_dat=_dat l,_first=_first l,_last=_last l}

goPrv :: DLinkedList a -> DLinkedList a
goPrv l = DLinkedList {_curr=nxt l,_dat=_dat l,_first=_first l,_last=_last l}

--add to list
add :: DLinkedList a -> a -> DLinkedList a

add v dl =
           let last_node = last_node dl in
           let count = count dl in
           if (count == 0) then
            let new_node = DLinkedListNode {_next=(-1),_prv=_last dl,_val = v} in
            let new_last_node = DLinkedListNode {_next=count,_prv=_prv last_node,_val=_val last_node} in
               DLinkedList {_curr= _curr dl, _dat= init(_dat dl):new_last_node:new_node,_first=_first dl,_last=_last dl }
           else
               -- We have an empty list ... first initialize it !
               let new_node = DLinkedListNode {_next=(-1),_prv=(-1),_val=0} in
               DLinkedList {_curr= 0, _dat= init(_dat dl):new_node,_first=0,_last=0 }

--List to LinkedList
listToDLinkedList  :: [a] -> DLinkedList a
listToDLinkedList l = mklist DLinkedList { _curr= (-1) , _dat= [],_first  = (-1),_last = (-1)  } l where
    mklist :: DLinkedList a -> [a] -> DLinkedList a
    mklist dl [] = dl
    mklist dl (v:t) = mklist (add v dl) t