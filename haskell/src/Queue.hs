module Queue where

data Queue t = Queue [t]

--Current item
current :: Queue t -> t
current (Queue (h:_)) = h

--Enqueue(Add)
enqueue :: t -> Queue t -> Queue t
enqueue val (Queue q) = Queue (q++[val])

--Dequeue(Remove)
dequeue :: Queue t -> Queue t
dequeue (Queue (_:t)) = Queue t
dequeue _ = Queue []

--Empty
empty :: Queue t -> Bool
empty (Queue []) = True
empty (Queue _) = False

--Make queue from list
makeQueue :: [t] -> Queue t
makeQueue list = mkQueue list (Queue []) where
    mkQueue :: [t] -> Queue t -> Queue t
    mkQueue [] q = q
    mkQueue (h:t) q = mkQueue t (enqueue h q)


--Print Queue
instance (Show t) => Show (Queue t) where
    show q = if empty q then "" else
             (show $ current q) ++ " => " ++ (show $ dequeue q)