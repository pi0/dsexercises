module Main where

import Stack

main = do
	putStrLn("Enter a word: ")
	s <- getLine
	putStrLn("Reversed form is: ")
	putStrLn(reverseStr s)

--Reverse String using Stack--
reverseStr :: [Char] -> [Char]
reverseStr str =
        put "" (listToStack str) where
        put :: [Char] -> Stack Char -> [Char]
        put r stck  = if empty stck
            then r
            else put (r ++ [top stck] ) $ pop stck