module Main where

import Queue

main = do
    putStrLn("Testing queue...")
    print(fill_q(makeQueue []) 0)

fill_q :: Queue Int->Int->Queue Int
fill_q q curr = if curr<5
    then fill_q (enqueue curr q) (curr+1)
    else q