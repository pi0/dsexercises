/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#include <iostream>
#include <string>
#include "../BigNum/BigNum.h"

int main() {

    BigNum result;//Default segmentation size is 3


    for(int i=1;i<=2;i++) {
        std::string input;
        std::cout<<"Enter number "<<i<<" : ";
        std::cin>>input;
        BigNum bigInput(input);
        result+=bigInput;
    }

    std::cout<<"Result :"<<result;

    return 0;
}