/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#include <iostream>
#include <string>
#include "../Stack/Stack.h"

bool syntaxCheck(std::string s);
int getOperatorCode(char o);

int main() {

    std::string input;
    std::cout<<"Input your equition:";
    std::cin>>input;

    if(syntaxCheck(input))
        std::cout<<"Valid syntax!";
    else
        std::cout<<"Invalid syntax!";

}

bool syntaxCheck(std::string s) {
    Stack<int> operatorsCodeStack;
    const char* cs=s.c_str();
    for(int i=0;cs[i]!=0;i++) {
        int code= getOperatorCode(cs[i]);
        if(!code)
            continue;
        if(!(code%2)) {
            //Open
            operatorsCodeStack.push(code);
        } else {
            //Close
            if(code/10!=operatorsCodeStack.popv()/10)
                return  false;
        }
    }
    return true;
}

char operatorsTable[][2]={{'{','}'},{'[',']'},{'(',')'},{0,0}};
int getOperatorCode(char o) {
    for(int i=0;operatorsTable[i][0]!=0;i++)
        if(o==operatorsTable[i][0])
            return (i+1)*10;
        else if(o==operatorsTable[i][1])
            return (i+1)*10+1;
    return 0;
}
