/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#include <iostream>
#include <sstream>

std::string getBin(int num);

int main() {

    std::cout << "Enter a number:";
    int num;
    std::cin >> num;
    std::cout << "Binary form is :" << getBin(num);

}

std::string getBin(int num) {
    std::string bin;
    do
        bin.insert(0, 1, (char) (num % 2 + '0'));
    while (num /= 2);
    return bin;
}
