/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#ifndef __StackIncluded
#define __StackIncluded

template<class T>
class Stack {

private :
    class StackNode {
    public:
        StackNode *next;
        T *value;

        StackNode(StackNode *next, T *value) : next(next), value(value) {
        }
    } *top = NULL;

public :
    T* pop() {
        if(isEmpty())
            return NULL;
        StackNode *curr = top;
        top = curr->next;
        return curr->value;
    }

    T& popv() {
        return *pop();
    }

    void clear() {
        while (pop());
    }

    void push(T *element) {
        StackNode *node = new StackNode(top, element);
        top = node;
    }

    void push (T &element){
        push(new T(element));
    }

    bool isEmpty() {
        return this->top == nullptr;
    }

};

#endif

