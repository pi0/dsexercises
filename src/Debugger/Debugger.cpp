#include "Debugger.h"

#include <stdio.h>

void _log(const char* tag,const char* msg) {

    printf("[%s] %s\n",tag,msg);

}