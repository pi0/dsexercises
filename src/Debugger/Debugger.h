#ifndef debugger
#define debugger

#define LOG(tag,a) _log(tag,(string()+a).c_str())
#define LOGE(a) LOG("ERROR",a)
#define LOGW(a) LOG("WARNING",a)
#define LOGI(a) LOG("INFO",a)

void _log(const char* tag,const char* msg);

#endif