/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include "BigNum.h"

BigNum::BigNum(int segmentDigits) : BigNum(*(new std::string()), segmentDigits) {
}

BigNum::BigNum(std::string &value, int segmentDigits) : segmentDigits(segmentDigits) {
    this->segmentMaxValue = (int) (pow(10, segmentDigits) - 1);
    initFromString(value);

}

void BigNum::initFromString(std::string &value) {
    std::string s;
    for (int i = (int) value.length(); i > 0; i -= segmentDigits) {
        if (i >= segmentDigits)
            s = value.substr(i - segmentDigits, segmentDigits);
        else
            s = value.substr(0, i);
        int val = atoi(s.c_str());
        segments.add(val);
    }
}


std::ostream &operator<<(std::ostream &os, const BigNum &num) {
    num.print(os, num.segments.getFirst());
    return os;
}

void BigNum::print(std::ostream &os, BigNumSegment *node) const {
    if (!node)
        return;
    print(os, node->next);

    if (node->next) {
        char *c=new char[segmentDigits + 1];
        sprintf(c, "%03d", (node)->value);
        os << c;
		delete[] c;
    } else if ((node)->value)
        os << (node)->value;

}


BigNum &BigNum::operator+(const BigNum &b) {
    BigNum *n = new BigNum();
    *n += *this;
    *n += b;
    return *n;
}

BigNum &BigNum::operator+=(const BigNum &b) {
    BigNum &a = *this;

    int carry = 0;
    for (BigNumSegment *bnode = b.segments.getFirst(), *anode = a.segments.getFirst(); bnode || carry; anode = anode->next) {

        if (!anode)
            anode = a.segments.add(0);

        //Add
        (anode)->value += carry;
        if (bnode) {
            anode->value += bnode->value;
            bnode = bnode->next;
        }
        //Carry
        if ((anode)->value > segmentMaxValue) {
            carry = (anode)->value / (segmentMaxValue + 1);
            (anode)->value -= (segmentMaxValue + 1);
        } else
            carry = 0;

    }

    return *this;
}
