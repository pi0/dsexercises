/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#ifndef __BigNumIncluded
#define __BigNumIncluded

#include <string>
#include <iostream>

#include "../LinkedList/LinkedList.h"

#define BigNumDefaultDigits 3

typedef LinkedList<int>::LinkedListNode BigNumSegment;

class BigNum {
private:
    int segmentDigits;
    int segmentMaxValue;
    LinkedList<int> segments;

    void initFromString(std::string &value);
    void print(std::ostream &os,BigNumSegment *node)const;

public:
    BigNum(int segmentDigits = BigNumDefaultDigits);
    BigNum(std::string &value,int segmentDigits = BigNumDefaultDigits);

    friend std::ostream& operator <<(std::ostream &os,const BigNum &num);
    BigNum& operator +(const BigNum& b);
    BigNum& operator += (const BigNum &b);

};

#endif