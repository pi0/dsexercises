#include <iostream>

#include "Controller.h"
#include "CommandParser.h"

using namespace std;

void demo(CommandParser&);

int main() {
    Controller c;
    CommandParser p(&c);

    demo(p);

    return 0;

    string inp;
    while(true){
        cout<<"CarStore > ";
        getline(cin,inp);
        if(!p.parseCommand(inp))
            break;
        cout<<endl;
    }
    cout<<endl;return 0;
}

void demo(CommandParser& p){

    p.parseCommand("add new service a1 5");
    p.parseCommand("add new service a1'");
    p.parseCommand("add new service a2");
    p.parseCommand("add new service a3");


    p.parseCommand("add subservice b1 to a2");
    p.parseCommand("add subservice b2 to a2");
    p.parseCommand("add subservice b3 to a2");
    p.parseCommand("add subservice c1 to a3 7");
    p.parseCommand("add subservice c2 to a3");


    p.parseCommand("add parent a1 to b1");
    p.parseCommand("add parent a1' to b1");
    p.parseCommand("add parent b1 to b3");

    p.parseCommand("delete a1'");

    p.parseCommand("list services");

//    p.parseCommand("list services from a2");

    p.parseCommand("add new agency 1:red");
    p.parseCommand("add new agency 2:blue");
    p.parseCommand("add new agency 3:green");
    p.parseCommand("add new agency 4:pink");

    p.parseCommand("list agency");

    p.parseCommand("Request b3 in red by pooya");
    p.parseCommand("Request c1 in red by \"pooya parsa\"");
    p.parseCommand("Request a1 in red by ali");

   // p.parseCommand("delete a3");

    p.parseCommand("list services");


    p.parseCommand("get requests red");
    p.parseCommand("get requests red");
    p.parseCommand("get requests red");


}

