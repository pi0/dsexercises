#include "CommandParser.h"
#include <sstream>
#include <stdlib.h>
#include <Debugger.h>

using namespace std;


bool CommandParser::parseCommand(const string &command) {
    string args[3];

    if (patternMatch("add new service *", command, args)) {

        c->addPrimaryService(args[0]);

    } else if (patternMatch("add new service * *", command, args)) {

        c->addPrimaryService(args[0], atoi(args[1].c_str()));

    } else if (patternMatch("add subservice * to *", command, args)) {

        c->addSubService(args[0], args[1]);

    }else if (patternMatch("add subservice * to * *", command, args)) {

        c->addSubService(args[0], args[1],atoi(args[2].c_str()));

    } else if (patternMatch("add parent * to *", command, args)) {

        c->addParent(args[0], args[1]);

    } else if (patternMatch("delete *", command, args)) {

        c->deleteService(args[0]);

    } else if (patternMatch("add new agency * *", command, args)) {

        c->addNewAgency(atoi(args[0].c_str()), args[1]);

    } else if (patternMatch("list agency", command, args)) {

        c->listAgency();

    } else if (patternMatch("list services from *", command, args)) {

        c->listServicesFrom(args[0]);

    } else if (patternMatch("list services", command, args)) {

        c->listServices();

    } else if (patternMatch("Request * in * by *", command, args)) {

        c->request(args[0], args[1], args[2]);


    } else if (patternMatch("get requests *", command, args)) {

        c->getRequests(args[0]);

    } else if (command == "exit" || command == "q") {

        return false;

    } else {

        LOGE("Command unrecognized : '" + command + "'");

    }

    return true;
}

bool caseInsensitiveStringCompare(const string &a, const string &b) {
    if (a.size() != b.size())
        return false;
    for (auto c1 = a.begin(), c2 = b.begin(); c1 != a.end(); c1++, c2++)
        if (tolower(*c1) != tolower(*c2))
            return false;
    return true;
}

string escapeCMD(string cmd) {

    bool qStarted = false;
    for (auto i = cmd.begin(); i != cmd.end(); i++) {
        if (*i == '"')
            qStarted = !qStarted;
        else if (qStarted && *i == ' ')
            *i = '%';
        else if (*i == ':')
            *i = ' ';
    }

    return cmd;
}

string unescapeArg(string arg) {
    for (auto i = arg.begin(); i != arg.end(); i++) {
        if (*i == '%')
            *i = ' ';
        else if (*i == '\"')
            *i = ' ';
    }
    return arg;
}

bool CommandParser::patternMatch(const string &pattern, const string &command, string *args_out) {

    stringstream ss1(pattern), ss2(escapeCMD(command));
    string sp1, sp2;
    int arg_c = 0;

    while (getline(ss1, sp1, ' ') && getline(ss2, sp2, ' '))
        if (sp1 == "*")
            args_out[arg_c++] = unescapeArg(sp2).c_str();
        else if (!caseInsensitiveStringCompare(sp1, sp2))
            return false;//Token mismatch

    return !getline(ss1, sp1, ' ') && !getline(ss2, sp2, ' ');
}