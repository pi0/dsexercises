#ifndef _ControllerhIncluded
#define _ControllerhIncluded

#include "LinkedList.h"
#include "Models/Service.h"
#include "Models/Agency.h"
#include <string>

using namespace std;

class Controller {

private:
    LinkedList<Service> services;
    typedef LinkedList<Service>::LinkedListNode ServiceNode;

    LinkedList<Agency> agencies;
    typedef LinkedList<Agency>::LinkedListNode AgencyNode;


public:

    //--Constructors--//

    Controller();

    //---Service related---//

    //Add new service
    //Returns created service or NULL if there is any error

    Service *addPrimaryService(const string &name,int priority=0);

    Service *addSubService(const string &name, const string &parent_name,int priority=0);

    //Add new parent to an exiting service
    //Returns true if there is no error
    bool addParent(const string &name, const string &parent_name);

    //Delete a service
    //Returns numbers of services (and sub-services) that removed
    int deleteService(const string &service_name);

    int deleteService(Service *service);

    //---Agency related---//

    //Adds a new agency
    //Returns true if there is no error
    bool addNewAgency(int id, const string &name);

    //Request service_name in agency_name by username
    //Returns true if there is no error
    bool request(const string &service_name, const string &agency_name, const string &by_username);

    //
    void getRequests(const string &agency_name);

    //---Misc---//

    Service *findServiceByName(const string &name, bool searchSubServices = true);

    Agency *findAgencyByName(const string &name);

    //
    void listAgency();

    //
    void listServices();

    //
    void listServicesFrom(const string &parent_name);

};


#endif