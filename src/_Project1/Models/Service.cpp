#include <ostream>
#include <iostream>

#include "Service.h"
#include "Debugger.h"

Service::Service(const string &name, int priority) : name(name), priority(priority) {

}


Service::~Service() {
    prepareDelete();

    for (ServiceNode *n = deleteList.getFirst(); n; n = n->next) {
        if (n->value == this)
            continue;
        delete n->value;
    }
}


LinkedList<Service *> Service::prepareDelete() {
    if (preparedToDelete)
        return deleteList;
    preparedToDelete = true;

    deleteList.add(this);

    for (ServiceNode *n = sub_services.getFirst(); n; n = n->next) {
        Service *s = n->value;
        if (s->first_parent == this)
            s->first_parent = NULL;
        if (s->num_of_parents-- <= 1) {//i am the last parent,so bye bye :D
            LinkedList<Service *> d2 = s->prepareDelete();
            deleteList.addAll(d2);
        }
    }

    return deleteList;
}

Service *Service::addSubService(Service *service) {
    if (!service) {
        LOGW("Adding NULL service ! Ignoreing ...");
        return NULL;
    }

    service->num_of_parents++;
    if (!service->first_parent)
        service->first_parent = this;

    return sub_services.add(service)->value;
}


bool Service::isPrimary() {
    return num_of_parents == 0;
}

int Service::compare(const Service *a, const Service *b) {
    if (a->priority != 0 || b->priority != 0) {
        if (a->priority > b->priority)
            return 1;
        else if (a->priority == b->priority)
            return 0;
        else return -1;
    }
    else return a->name.compare(b->name);
}

bool Service::equals(const Service *a, const Service *b) {
    return a->name == b->name;
}


bool Service::equals(const Service *b) {
    return equals(this, b);
}


Service *Service::find(const Service *service, bool recursive, bool recurseOnFirstParentsOnly) {

    if (equals(service))
        return this;

    for (ServiceNode *n = sub_services.getFirst(); n; n = n->next) {

        if (!n->value->first_parent)
            n->value->first_parent = this;

        if (n->value->equals(service))
            return n->value;

        //DFS
        if (recursive && (!recurseOnFirstParentsOnly || this == n->value->first_parent)) {
            Service *tmp = n->value->find(service, recursive/*true*/);
            if (tmp)
                return tmp;
        }

    }
    return NULL;
}

void Service::clearReferencesTo(Service *srv) {
    bool removed;
    do {
        removed = false;
        for (ServiceNode *n = sub_services.getFirst(), *p = NULL; n; p = n, n = n->next)
            if (n->value == srv) {
                sub_services.remove_node(n, p);
                removed = true;
                break;
            } else
                n->value->clearReferencesTo(srv);
    } while (removed);
}

void Service::print(ostream &s, string prefix) {
    s << prefix + " + " << name ;
    if(priority)
        s<<" {priority :"<<priority<<"}";
    s<<endl;

    for (ServiceNode *n = sub_services.getFirst(); n; n = n->next) {
        if (!n->next)
            n->value->print(s, prefix + "    ");
        else
            n->value->print(s, prefix + "   |");
    }
}

