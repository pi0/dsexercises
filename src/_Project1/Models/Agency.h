#ifndef _AgencyehIncluded
#define _AgencyehIncluded

#include <string>
#include "Queue.h"
#include "Service.h"

class Agency {

public :
    class Request {
    public:
        Service *service;
        std::string username;

        Request(Service *service, string username)
                : service(service), username(username) {
        }

        static int compare(const Request *a, const Request *b) {
            int c = Service::compare(a->service, b->service);
            if (!c)
                return a->username.compare(b->username);
            else return c;
        }
    };

private:
    int id;
    const std::string name;

    Queue<Request> requestes;

public:



    Agency(const string &name, int id = -1) : name(name), id(id) {
    }

    int getId() const {
        return id;
    }

    string getName() const {
        return name;
    }

    void doRequest(Service *service, const string &username);

    void printRequests();

    Request* serveRequest();

    static int compare(const Agency *a, const Agency *b);

    void removeRequests(Service *srv);


};


#endif