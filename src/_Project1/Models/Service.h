#ifndef _ServicehIncluded
#define _ServicehIncluded

#include <string>
#include "LinkedList.h"

using namespace std;

class Service {

private:
    string name;
    LinkedList<Service*> sub_services;
    Service *first_parent = 0;
    int num_of_parents = 0;

    bool preparedToDelete=false;
    LinkedList<Service *> deleteList = LinkedList<Service *>();

public:

    string targets;
    string description_public;
    string description;
    int requiredTime;
    int requiredCost;

    int priority;

	string getName() const {
		return name;
	}


    //--Constructors--//

    Service(const string &name,int priority=0);

    ~Service();

    LinkedList<Service*> prepareDelete();

    //--Misc--//

    Service *addSubService(Service *service);

    bool isPrimary();

    static int compare(const Service *a,const Service *b);

    static bool equals(const Service *a,const Service *b);

    bool equals(const Service *b);

    Service *find(const Service* service, bool recursive=true,bool recurseOnFirstParentsOnly=true);

    void print(ostream& s,string prefix);

    void clearReferencesTo(Service* srv);

    typedef LinkedList<Service *>::LinkedListNode ServiceNode;
};

#endif