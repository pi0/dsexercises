#include <iostream>
#include "Agency.h"

void Agency::printRequests() {
    cout << "Requests to agency :" << name << endl;

    for (auto s = requestes.getData().getFirst(); s; s = s->next) {
        cout << "  " << s->value.service->getName() << "(priority:"<<s->value.service->priority << ")"<< " by " << s->value.username << endl;
    }
}

Agency::Request* Agency::serveRequest(){
    return requestes.dequeue();
}

void Agency::doRequest(Service *service, string const &username) {
    requestes.enqueueOrdered(new Request(service, username), Request::compare);
}

int Agency::compare(const Agency *a, const Agency *b) {

    if (a->id < 0 || b->id < 0) {
        //Compare names instead of id
        return a->name.compare(b->name);
    } else
    if (a->id == b->id)
        return 0;
    else if (a->id > b->id)
        return 1;
    else return -1;
}

void Agency::removeRequests(Service* srv) {
    bool removed;
    do {
        removed = false;
        for (LinkedList<Request>::LinkedListNode* r = requestes.getData().getFirst(), *p = NULL; r; p = r, r = r->next) {
            if (r->value.service == srv) {
                removed = true;
                requestes.getData().remove_node(r, p);
                break;
            }
        }
    }while(removed);
}
