#include <iostream>

#include "Debugger.h"
#include "Controller.h"

using namespace std;

Controller::Controller() {

}

Service *Controller::addPrimaryService(const string &name,int priority) {
    if (findServiceByName(name)) {
        LOGE("Service already exists : " + name);
        return 0;
    }
    return &services.add(new Service(name,priority))->value;
}


Service *Controller::addSubService(const string &name, const string &parent_name,int prioriry) {
    Service *parent = findServiceByName(parent_name);
    if (!parent) {
        LOGE("Parent not found: " + parent_name);
        return 0;
    }
    return parent->addSubService(new Service(name,prioriry));
}


bool Controller::addParent(const string &parent_name, const string &name) {
    Service *parent = findServiceByName(parent_name, true);
    Service *service = findServiceByName(name, true);

    if (!parent || !service) {
        LOGE("Service not found: " + (!parent ? parent->getName() : service->getName()));
        return false;
    }

    //TODO circular parenting check !

    parent->addSubService(service);
    return true;
}

int Controller::deleteService(const string &service_name) {
    Service *s = findServiceByName(service_name, true);
    if (!s) {
        LOGE("Service not found: " + service_name);
        return 0;
    }
    return deleteService(s);
}


int Controller::deleteService(Service *service) {

    if (service->isPrimary())
        services.remove(Service::compare, service);

    LinkedList<Service *> deletedSubServices = service->prepareDelete();

    for (Service::ServiceNode *n = deletedSubServices.getFirst(); n; n = n->next) {
        LOGI("Removing service "+n->value->getName()+" and any refrence to it ...");
        //Remove All agencies requests to that service
        for(AgencyNode* a=agencies.getFirst();a;a=a->next)
            a->value.removeRequests(n->value);
        for (ServiceNode *s = services.getFirst(); s; s = s->next)
            s->value.clearReferencesTo(service);
    }

    delete service;

    cout<<"Total removed services : "<<deletedSubServices.size()<<endl;

    return deletedSubServices.size();
}


Service *Controller::findServiceByName(const string &name, bool searchSubServices) {
    Service templateSrv(name);
    for (ServiceNode *s = services.getFirst(); s; s = s->next) {
        Service *found = s->value.find(&templateSrv, searchSubServices);
        if (found)
            return found;
    }
    return NULL;
}

Agency *Controller::findAgencyByName(const string &name) {
    Agency templ = Agency(name);
    AgencyNode *an = agencies.find(Agency::compare, &templ);
    if (an)
        return &an->value;
    return NULL;
}


bool Controller::addNewAgency(int id, const string &name) {
    Agency *new_ag = new Agency(name, id);
    AgencyNode *agencyNode = agencies.find(Agency::compare, new_ag);
    if (agencyNode) {
        LOGW("Agency " + name + " already exists ! (Ignored)");
        delete new_ag;
        return false;
    }
    agencies.add(new_ag);
    return true;
}

bool Controller::request(const string &service_name, const string &agency_name, const string &by_username) {

    Agency *agency = findAgencyByName(agency_name);
    if (!agency) {
        LOGE("Agency not found : " + agency_name);
        return false;
    }

    Service *s = findServiceByName(service_name);
    if (!s) {
        LOGE("Service not found: " + service_name);
        return false;
    }

    agency->doRequest(s, by_username);
    return true;
}

void Controller::getRequests(const string &agency_name) {

    Agency *agency = findAgencyByName(agency_name);
    if (!agency) {
        LOGE("Agency not found : " + agency_name);
        return;
    }
    agency->printRequests();

    Agency::Request* r=agency->serveRequest();

    cout<<"Now serving service "<<r->service->getName()<<" for user "<<r->username<<endl;


}

bool print_agency(Agency *a) {
    cout << a->getName() << "(" << a->getId() << ")" << endl;
    return true;
}

void Controller::listAgency() {
    cout << "Current agencies :" << endl;
    agencies.foreach(print_agency);
}

void Controller::listServices() {
    for (ServiceNode *p = services.getFirst(); p; p = p->next)
        p->value.print(cout, "");
}

void Controller::listServicesFrom(const string &parent_name) {
    Service *s = findServiceByName(parent_name);
    if (!s) {
        LOGE("Service not found: " + parent_name);
        return;
    }
    s->print(cout, "");
}

