#ifndef _CPINC
#define _CPINC

#include "Controller.h"
#include <string>

#include <iostream>


class CommandParser {

private:
    Controller *c;

public :

    CommandParser(Controller *bind_to) {
		c = bind_to;
    }

    bool parseCommand(const std::string& command);

	bool patternMatch(const string &pattern, const string &command, string *args_out);

};


#endif