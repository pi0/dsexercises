/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */
 
#include <iostream>
#include "Student.h"
#include <sstream>


Student *Student::fromCLI() {
    using namespace std;
    const char* n="";

    Student *s = new Student(n, 0, 0, 0);

    cout << "Enter student info: format : name Num gender(0:female 1:male) score" << endl;

    cin >> s->name >> s->studentNum >> s->gender >> s->score;

    return s;
}

int Student::compareStudent(Student *a, Student *b) {
    if (a->score > b->score)
        return 1;
    if (a->score < b->score)
        return -1;
    else
        return 0;
}


std::string Student::getGenderStr() {
    return gender ? "Male" : "Female";
}

std::string toString(int a){
    std::stringstream  ss;
    ss<<a;
    return ss.str();
}

std::string **Student::getFields() {

    std::string** fields=new std::string*[5];

    std::string g= getGenderStr();
    int i=0;

    fields[i++]=&name;
    fields[i++]= new std::string(toString(score));
    fields[i++]=new std::string(getGenderStr());
    fields[i++]= new std::string(toString(studentNum));

    fields[i]= nullptr;

    return fields;
}

void Student::print() {

	const char* fields_name[] = { "Name", "Score", "Gender", "Student Number" };

    std::string** fields= getFields();
    std::cout<<"{\n";
    for(int i=0;fields[i];i++){
        std::cout<<"    "<<fields_name[i]<<" : "<<*fields[i]<<",\n";
    }
    std::cout<<"}\n";
}


bool Student::doPrintStudent(Student *std) {
    std->print();
    return true;
}

bool StudentContainer::print(StudentContainer *c) {
    std::cout << "\nStudents with score " << c->score << " [ \n";
    c->students.foreach(Student::doPrintStudent);
    std::cout << "\n]\n";
    return true;
}

int StudentContainer::compareContainer(StudentContainer *a, StudentContainer *b) {
    if (a->score > b->score)
        return 1;
    if (a->score < b->score)
        return -1;
    else
        return 0;
}
