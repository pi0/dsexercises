/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */
 
#ifndef __StudentIncluded__
#define __StudentIncluded__

#include "../LinkedList/LinkedList.h"
#include <string>

class Student {

private:
    

public:
    std::string name;
    int studentNum;
    int gender;
    int score;

    Student(const char *name, int studentNum, int gender, int score)
            : name(name), studentNum(studentNum), gender(gender), score(score) {
    }

    void print();

    static bool doPrintStudent(Student *std);

    static int compareStudent(Student *a, Student *b);

    std::string getGenderStr();

    static Student* fromCLI();

    std::string** getFields();
};

typedef LinkedList<Student>::LinkedListNode StudentNode;

class StudentContainer {
public:
    int score;

    StudentContainer(int score) : score(score) {
    }

    LinkedList<Student> students;
    static bool print(StudentContainer *c);
    static int compareContainer(StudentContainer *a, StudentContainer *b);
};

#endif