/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */
 
#ifndef __StudentManagerIncluded__
#define __StudentManagerIncluded__

#include "../LinkedList/LinkedList.h"
#include "Student.h"


typedef LinkedList<StudentContainer>::LinkedListNode StudentsManagerNode;

class StudentsManager {

private:
    LinkedList<StudentContainer> students;
    bool descending= false;


public:


    StudentsManager(bool descending) : descending(descending) {
    }

    static StudentsManager *fromString(std::string &list,bool descending=false);

    void addStudent(Student *std);

    void printList();

    void interactiveCLI();

    StudentsManagerNode * findByScore(int score);
};


#endif