/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#include <string>
#include <iostream>
#include "StudentsManager.h"

#include "../Debugger/Debugger.h"

int main(void) {
    using namespace std;

    bool descending = false;
    cout << "Enter storage mode (1:accending 0:descending)  : ";
    cin >> descending;
    descending = !descending;


    StudentsManager *studentsManager = NULL;

    int mode = 0;
    while (!mode) {
        cout << "\ndb init mode : " << endl << endl <<
                "1) Use simple data" << endl <<
                "2) json like string" << endl <<
                "3) Interactive input" << endl;
        cout << endl << " : ";
        cin >> mode;

        switch (mode) {
            case 1:
            case 2:
                string *input;
                while (!studentsManager) {
                    if (mode == 1)
                        input = new string("{(A,2),(B,11),(C,8),(D,16),(E,14),(F,15),(G,17),(H,17),(I,17),(J,18)}");
                    else {
                        cout << "Input data in format : {(name,score),(name2,score2),...} : \n > ";
                        cin >> *(input = new string());
                    }
                    studentsManager = StudentsManager::fromString(*input, descending);
                    if (!studentsManager)
                        cout << "Syntax error!\n";
                }
                break;
            case 3:
                int n;
                studentsManager = new StudentsManager(descending);
                cout << "Enter number of students : ";
                cin >> n;
                while (n-- > 0) {
                    studentsManager->addStudent(Student::fromCLI());
                }
                break;
            default:
                break;
        }

    }

    std::cout << "\n______________\n<Students list>";
    studentsManager->printList();
    std::cout << "\n______________\n";


    studentsManager->interactiveCLI();

    return 0;
}

