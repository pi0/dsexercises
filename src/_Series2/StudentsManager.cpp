/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */
 
#include <iostream>
#include <stdlib.h>
#include <string>
#include "StudentsManager.h"

void StudentsManager::addStudent(Student *std) {
    //First find it's container
    StudentsManagerNode *n = findByScore(std->score);
    StudentContainer *c;
    if (!n)
        c = &students.addSorted(new StudentContainer(std->score), StudentContainer::compareContainer, descending)->value;
    else
        c = &n->value;

    c->students.add(std);//Students are not sorted in container
}

StudentsManagerNode *StudentsManager::findByScore(int score) {

    StudentsManagerNode *curr = students.curr();

    if (!curr)
        return NULL;

    int currScore = curr->value.score;

    StudentsManagerNode *start = ((descending && currScore >= score) || (!descending && currScore <= score))
            ? curr : NULL/*Starts from first*/;

    return students.find(StudentContainer::compareContainer, new StudentContainer(score), start, false/*don't wrap*/);
}



void StudentsManager::printList() {
    students.foreach(StudentContainer::print);
}

void StudentsManager::interactiveCLI() {
    using namespace std;

    string command;

    students.seekFirst();

    while (true) {

        cout<< "Current Group :\n";
        StudentContainer::print(&students.curr()->value);

        cout << "Enter command : ";
        cin >> command;

        if (command[0] == 'e')
            break;
        switch (command[0]) {
            //Seek commands
            case 'n':
                    students.next();
                break;
            case 'p':
                    students.prev();
                break;
            case 'f':
                    int score;
                    cin>>score;
                    students.seek(findByScore(score));
                break;
            default:
                cout << "Error : unrecognized command !\n";
                break;
        }
    }
}


StudentsManager *StudentsManager::fromString(std::string &list,bool descending) {

    //{(NAME,score),(NAME2,score)}

    StudentsManager *sm = new StudentsManager(descending);

    unsigned long len = list.length();
    char *str = new char[len];
    list.copy(str, len, 0);

    if (!str[0] == '{' || !str[len - 1] == '}')
        return NULL;

    char *nameStart = NULL,
            *scoreStart = NULL;

    int status = 0;//0:ready 1:name 2:score

    for (int i = 1; i < len - 1; i++)
        if (str[i] == '(' && status == 0) {
            status = 1;//Name
            nameStart = str + i + 1;
        } else if ((str[i] == ',' || ((str[i] >= 'A' && str[i] <= 'Z')||((str[i] >= 'a' && str[i] <= 'z'))) ) && status == 1) {
            if (str[i] == ',') {
                status = 2;//Score
                scoreStart = str + i + 1;
                str[i] = 0;//terminate name string
            }
        } else if ((str[i] == ')' || (str[i] >= '0' && str[i] <= '9')) && status == 2) {
            if (str[i] == ')') {
                status = 0;//Ready
                str[i] = 0;
                //Add item
                int score = atoi(scoreStart);

                sm->addStudent(new Student(nameStart, 0, 0, score));

                if (str[i + 1] != ',')
                    return sm;//TODO
                else str += 1;
            }
        } else {
            return NULL;
        }

    delete[] str;

    return sm;
}