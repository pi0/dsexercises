/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */


#ifndef __LinkedListIncluded
#define __LinkedListIncluded

#ifndef NULL
#define NULL 0
#endif

#include "Stack.h"

template<class T>
class LinkedList {

public :

    class LinkedListNode {
    public:
        LinkedListNode *next;
        T &value;

        LinkedListNode(LinkedListNode *next, T &value) : next(next), value(value) {
        }

        LinkedListNode(LinkedListNode *next, T *value) : LinkedListNode(next, *value) {
        }

    };

private:
    int _size = 0;
    LinkedListNode *first = NULL, *last = NULL, *current = NULL;
    Stack<LinkedListNode> history;


public:

    int size() const {
        return _size;
    }

    LinkedListNode *addNode(LinkedListNode *node) {
        if (!first)
            first = last = node;
        else {
            last->next = node;
            last = last->next;
            last->next = NULL;
        }
        _size++;
        return node;
    }

    LinkedListNode *insertNode(LinkedListNode *node, LinkedListNode *after) {

        if (after == last)
            return addNode(node);

        if (after) {
            node->next = after->next;
            after->next = node;
        } else {
            //If after is NULL then add it to first of list
            node->next = first;
            first = node;
        }

        return node;
    }


    LinkedListNode *add(T *value) {
        LinkedListNode *node = new LinkedListNode(NULL, value);
        return addNode(node);
    }

    LinkedListNode *add(T value) {
        return add((T *) (new T(value)));
    }

    LinkedListNode *addSorted(T *value, int (*comp)(const T *, const T *), bool descending = false) {
        LinkedListNode *node = new LinkedListNode(NULL, value),
                *n = first, *p;

        for (p = NULL; n && comp(value, &n->value) == (descending ? -1 : +1); p = n, n = n->next);
        insertNode(node, p);

        return node;
    }

    LinkedListNode *find(int (*comp)(const T *, const T *), T *searchFor, LinkedListNode *start = NULL, bool wrapSearch = true) {

        LinkedListNode *c;
        bool wrapped = false;

        if (!start && wrapSearch)
            wrapSearch = false;

        for (c = start ? start : first; c; c = c->next) {

            bool dummy = c == start;

            if (comp(searchFor, &c->value) == 0) {
                return c;

            }

            if (c == last && wrapSearch) {
                c = first;
                wrapped = true;
            }
            if (wrapped && c == start)
                break;
        }


        return NULL;

    }

    int remove(int (*comp)(const T *, const T *), T *searchFor, bool removeAll = false) {

        if (removeAll) {
            int i = 0;
            while (remove(comp, searchFor, false))i++;
            return i;
        }

        LinkedListNode *c, *p = NULL;
        for (c = first; c; p = c, c = c->next)
            if (comp(&c->value, searchFor) == 0)
                break;
        if (!c)
            return 0;
        remove_node(c, p);
        return 1;
    }

    void remove_node(LinkedListNode *n, LinkedListNode *p) {
        if (!p) {
            first = first->next;
        } else if (!n->next) {
            last = p;
            last->next = NULL;
        } else {
            p->next = n->next;
        }
        delete n;
    }

    LinkedListNode *getFirst() const {
        return first;
    }


    void setFirst(LinkedListNode *first) {
        LinkedList::first = first;
    }

    void seek(LinkedListNode *node) {
        //Clear history on long jumps
        //if(!areNodesConnected(current, node))
        //history.clear();
        //seek
        current = node;
    }

    void seekFirst() {
        seek(first);
    }

    LinkedListNode *curr() {
        if (!current && first)
            seekFirst();//Cycle !
        return current;
    }

    LinkedListNode *next() {
        if (!current)
            return NULL;
        //We don't use curr() as it behaves like a circular list !
        history.push(current);
        return (current = current->next);
    }


    LinkedListNode *prev() {
        if (!history.isEmpty())
            return (current = history.pop());
        return NULL;
    }

    void addAll(const LinkedList<T> &b) {
        for (LinkedListNode *n = b.getFirst(); n; n = n->next)
            add(n->value);
    }

    void foreach(bool (*action)(T *item)) {
        for (LinkedListNode *n = first; n; n = n->next)
            if (!action(&n->value))
                break;
    }

    static bool areNodesConnected(LinkedListNode *a, LinkedListNode *b) {
        return (a && a->next == b) || (b && b->next == a);
    }


};

#endif