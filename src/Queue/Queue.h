/*
This file is a part of my university exercises for DataStructures & Algorithms course
Written by pooya parsa ( pyapar[#]gmail.com )
Hope you learn something from this code && DON'T COPY-PASTE it
 */

#ifndef __QueuehIncluded
#define __QueuehIncluded

#include "LinkedList.h"

template<class T>
class Queue {

private:
    LinkedList<T> data;

public:

    void enqueue(T* item){
        data.add(item);
    }

    void enqueueOrdered(T* item,int (*comp)(const T *, const T *)){
        data.addSorted(item,comp,true);
    }

    T* dequeue(){
        auto f=data.getFirst();
        if(!f)
            return 0;
        data.setFirst(f->next);
        T* t=&f->value;
        delete f;
        return t;
    }

    int size(){
        return data.size();
    }


    LinkedList<T> &getData() {
        return data;
    }
};

#endif